from socket import inet_aton, inet_ntoa
import SocketServer
from struct import *
# import sys
import datetime
import random
from db_models import Peers
from mongoengine import connect
# import pdb


import string
# basically what could be a version number for a peer e.g. -AZ530- or -DE1360-
printable = string.letters + string.digits + '-'
def hexify(s):
    return ''.join(c if c in printable else r'{0:02x}'.format(ord(c)) for c in s)


def ip2int(addr):                                                               
    return unpack('!I', inet_aton(addr))[0]


def int2ip(addr):
    return inet_ntoa(pack('!I', addr))

connect('tracker')


class MyUDPHandler(SocketServer.BaseRequestHandler):

    def handle(self):
        data = self.request[0]
        req_socket = self.request[1]
        # print '%s: %d: %s' % (self.client_address[0], len(data), str([ord(c) for c in data]))
        if len(data) >= 16:
            con_id, action, trans_id = unpack_from('!Qii', data)
            response = pack('!ii5s', 3, trans_id, 'error')
            # print unpack_from('!qii', data)
            # The connect (need to redo)
            if action == 0 and con_id == 0x41727101980:
                while True:
                    con_id = random.getrandbits(63)
                    if con_id != 0x41727101980:
                        break
                response = pack('!iiQ', action, trans_id, con_id)
            # Announce
            elif action == 1 and len(data) >= 98:
                info_hash, peer_id, downloaded, left, uploaded, event, ip, key, num_want, port = unpack_from('!20s20sqqqiIiiH', data, 16)
                # print '%s: announce %s' % self.client_address, unpack_from('!20s20sqqqiIiiH', data, 16)
                completed = 1 if event == 1 or left == 0 else 0
                # Prefer reading Hex instead of Bindary Base64 in DB
                info_hash = info_hash.encode('hex')
                peer_id = hexify(peer_id)
                # Send a max of 200 (MTU is like ~1400-1500 bytes)
                num_want = min(200, num_want) if num_want >= 0 else 200
                # String ips are more readable
                ip = self.client_address[0] if ip == 0 else int2ip(ip)
                # default port?
                port = self.client_address[1] if port == 0 else port

                # Contact every 10 mins
                interval = 600
                # Aggregate the leechers and seeders
                leechers = Peers.objects(info_hash=info_hash, status=0).count()
                seeders = Peers.objects(info_hash=info_hash, status=1).count()

                # add the basic reponse before adding ips
                ips_ports = [pack('!iiiii', 1, trans_id, interval, leechers, seeders)]

                # Get a list of peers (depending on what u want more seeds or leechers)
                if completed:
                    cursor = Peers.objects(info_hash=info_hash, peer_id__ne=peer_id).order_by('-status').only('ip','port').limit(num_want)
                else:
                    cursor = Peers.objects(info_hash=info_hash, peer_id__ne=peer_id).order_by('status').only('ip','port').limit(num_want)
                ips_ports.extend( (pack('!IH', ip2int(peers.ip), peers.port) for peers in cursor) )

                # Get our peer (for now ignore key)
                peer_exists = Peers.objects(peer_id=peer_id, info_hash=info_hash).first()
                # Update the peer
                if peer_exists is None:
                    Peers(peer_id=peer_id, info_hash=info_hash, status=completed, downloaded=downloaded,
                                        left=left, uploaded=uploaded, ip=ip, key=key, port=port).save()
                else:
                    peer_exists.status = completed
                    peer_exists.downloaded = downloaded
                    peer_exists.left = left
                    peer_exists.uploaded = uploaded
                    peer_exists.ip = ip
                    peer_exists.key = key
                    peer_exists.port = port
                    # protection against someone trying to delete the swarm
                    if event != 3:
                        peer_exists.last_announced = datetime.datetime.utcnow()
                    peer_exists.save()
                # join all the stuff!
                response = ''.join(ips_ports)
            # The scrape
            elif action == 2:
                num = (len(data) - 16) / 20
                # start building the response
                scrape = [pack('!ii', 2, trans_id)]
                for i in xrange(num):
                    info_hash = unpack_from('!20s', data, 16 + 20 * i)[0]
                    info_hash = info_hash.encode('hex')
                    leechers = Peers.objects(info_hash=info_hash, status=0).count()
                    seeders = Peers.objects(info_hash=info_hash, status=1).count()
                    # 0 for # of torrent downloads (for now)
                    scrape.append(pack('!iii', seeders, 0, leechers))
                response = ''.join(scrape)
            # print '%d: %s' % (len(response), str([hex(ord(c)) for c in response]))
            # print '\n'
            # sys.stdout.flush()
            req_socket.sendto(response, self.client_address)

if __name__ == '__main__':
    HOST, PORT = '', 6969
    server = SocketServer.UDPServer((HOST, PORT), MyUDPHandler)
    server.serve_forever()
