from mongoengine import *
import datetime


class Peers(Document):
    peer_id = StringField(required=True)
    info_hash = StringField(required=True)
    status = IntField()
    downloaded = LongField()
    left = LongField()
    uploaded = LongField()
    ip = StringField()
    key = IntField()
    port = IntField()
    last_announced = DateTimeField(default=datetime.datetime.utcnow)
    meta = {
        'indexes': [('peer_id', 'info_hash', 'key'), ('info_hash', 'status'),
                    {'fields': ['last_announced'], 'expireAfterSeconds': 660}
                    ]
    }




